# Predicting Response Threshold of Peanut Allergy Provocations

This research is conducted by Martijn van der Werff and Dennis Scheper

---
_Sprint period 1 and 2_

In this repository, you find an Exploratory Data Analysis (EDA) log on how data regarding predicting the height of the response thresholds of provocations - both negatives and positives - was cleaned, restructured, and visualized to make it ready for machine learning processing. After the EDA process, the number of attributes was significantly reduced, as the number of patients were many times smaller than the amount of attributes. This imbalance cased models to overfit on the small amount of instances. We performed attribute selection in [Weka][weka] (version 3.0). After attribute selection was completed, a varies amount of attributes remained, as a dataset with only positive provocations and another containing both types were made, that performed better with the amount of instances present.

_Sprint period 3 and 4_

Data augmentation was implemented to deal with the low amount of instances present in the data. Data augmentation was done in Python, as were other analyses surrounding data augmentation, and its results are explained in Jupyter notebooks under `data_aug/` for each Sprint period. As a result of data augmentation, algorithm performance increased, producing models that predict better. Correlation between attributes was not accounted for in the first method of data augmentation; a new version would, and produced better results. Different strategies were employed to merge the microbiome and peanut dataset on patients' IDs. The final addition was the mean per bacteria phyla to the peanut dataset. Patients who did not have records of their microbiome were removed, and due to this loss of instances, algorithms' accuracy of predictions went down. Data augmentation was once again employed on the merged dataset and produced better results than any other scenario. PCA was performed on all of the food intakes, which resulted in a more simplified model that performed only slightly worse in comparison with other used datasets. In addition, we looked more thoroughly at the biological relevancy of remaining attributes that were present in the final sets after attribute selection. Results of significance retrieved from the client were also reproduced and reported on.

# Setup
Clinical data was retrieved from the client and used as test and training data regarding machine learning. We used [Rstudio][r] (version 4.0.2) to clean, restructure, and visualize data and findings. For that, we used a couple of external packages/libraries:

- [plyr (1.8.6)][plyr]
- [dplyr (1.0.2)][dplyr]
- [car (3.0-10)][car]
- [labelled (2.8.0)][labelled]
- [tidyverse (1.3.0)][tidyverse]
- [visdat (0.5.3)][visdat]
- [haven (2.4.0)][haven]
- [grid (3.6.2)][grid] and [gridExtra (2.3)][gridExtra]
- [kableExtra (1.3.1)][kableExtra]
- [naniar (0.6.0)][naniar]
- [caret (6.0-86)][caret]
- [pals (1.6)][pals]
- [ggplot2 (3.3.2)][ggplot2] and [ggbiplot (0.55)][ggbiplot]
- [ggpubr (0.4.0)][ggpubr]
- [readxl (	1.3.1)][readxl]
- [reshape2 (1.4.4)][reshape2]
- [scatterplot3d (0.3-41)][scatterplot]
- [waffle (0.7.0)][waffle]
- [scales (1.1.1)][scales]

As stated before, [Weka](weka) was used to evaluate the initial datasets on a multitude of algorithms and to reduce the large amount of attributes by selection. Data augmentation was performed in [Python][python] (version 3.7.9) with multiple packages, which are available in `requirements.txt` under each of the `data_aug` subdirectories. Some essential packages include [Pandas][pandas] (version 1.2.4) for data manipulation, [scikit-learn][sk] (version 0.24) regarding machine learning, [NumPy][numpy] (version 1.20.0) for manipulating data structures, and [scikitplot][sciplot] (version 0.3.7) plotting training and validation curves. Installation can be done by first setting up a virtual environment, although not required. Thereafter, simply execute the following command on the command-line:

```bash
$ pip install -r requirements.txt
```

# Layout
In the root of the repository is a progress report located that sums up the findings, which problems came across, and new goals regarding our research. Subdirectories contain logs that go into detail about how the research was conducted, why certain choices were made with, for example, attribute selection, and more. The log is focused on being reproducible. The subdirectories are set up as follows:

- `EDA/`: contains a log regarding the EDA process with visualizations and explanations about the data structure, our discoveries in the data, and how the data was prepared for machine learning.
- `weka/`: contains a log regarding the attribute selection process which was performed in Weka. Performance of algorithms before and after attribute selection, and which techniques used during attribute selection were reported on.
- `sprint_3`: contains a log regarding the data augmentation techniques used, the merging of both datasets, and reproducing some of our client's results. Data augmentation results are discussed in a Jupyter notebook with some modules that perform data augmentation (`data_aug.py`) and evaluates the results (`evaluate.py`), which are available under subdirectory `data_aug/`.
- `sprint_4`: contains a log regarding PCA, biological relevancy of remaining attributes, and a data augmentation that is extended to account for correlation between attributes, which was a weakness in the previous version. The old and new methods are compared and discussed with one another in multiple situations. This part is also available in a Jupyter notebook under the subdirectory `data_aug/`.
- `misc/`: contains some figures, codebook(s) and other miscellaneous objects.

# Contact
If any issue or question remains, please contact us at [d.j.scheper@st.hanze.nl](mailto:d.j.scheper@st.hanze.nl) or [m.w.van.der.werff@st.hanze.nl](mailto:m.w.van.der.werff@st.hanze.nl).

[weka]: https://www.cs.waikato.ac.nz/ml/weka/
[r]: https://www.rstudio.com/
[plyr]: https://www.rdocumentation.org/packages/plyr/versions/1.8.6
[dplyr]: https://dplyr.tidyverse.org/
[car]: https://cran.r-project.org/web/packages/car/index.html
[labelled]: https://cran.r-project.org/web/packages/labelled/index.html
[tidyverse]: https://www.tidyverse.org/
[visdat]: https://cran.r-project.org/web/packages/visdat/index.html
[grid]: https://www.rdocumentation.org/packages/grid/versions/3.6.2)
[gridExtra]: https://cran.r-project.org/web/packages/gridExtra/index.html
[haven]: https://haven.tidyverse.org/
[kableExtra]: https://cran.r-project.org/web/packages/kableExtra/index.html
[naniar]: https://cran.r-project.org/web/packages/naniar/index.html
[caret]: http://topepo.github.io/caret/index.html
[pals]: https://cran.r-project.org/web/packages/pals/vignettes/pals_examples.html
[ggplot2]: https://ggplot2.tidyverse.org/
[ggbiplot]: https://www.rdocumentation.org/packages/ggbiplot/versions/0.55
[ggpubr]: http://www.sthda.com/english/articles/24-ggpubr-publication-ready-plots/
[readxl]: https://readxl.tidyverse.org/
[reshape2]: https://www.rdocumentation.org/packages/reshape2/versions/1.4.4
[scatterplot]: https://cran.r-project.org/web/packages/scatterplot3d/scatterplot3d.pdf
[waffle]: https://cran.r-project.org/web/packages/waffle/waffle.pdf
[scales]: https://cran.r-project.org/web/packages/scales/index.html
[python]: https://www.python.org/
[pandas]: https://pandas.pydata.org/
[sk]: https://scikit-learn.org/stable/
[numpy]: https://numpy.org/
[sciplot]: https://scikit-plot.readthedocs.io/en/stable/Quickstart.html