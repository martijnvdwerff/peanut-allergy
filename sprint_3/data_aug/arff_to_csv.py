#!/usr/bin/env python3

"""
Script to write arff to csv.
"""

# Information
__author__ = "Martijn van der Werff (345772) and Dennis Scheper (373689)"
__date__ = "07/05/2021"
__type__ = "Script"

# Imports
import os

# Functions
def toCsv(text):
    """
    Function used to convert ARFFs to CSVs.
    """
    data = False
    header = ""
    new_content = []
    for line in text:
        if not data:
            if "@ATTRIBUTE" in line or "@attribute" in line:
                attributes = line.split()
                if("@attribute" in line):
                    attri_case = "@attribute"
                else:
                    attri_case = "@ATTRIBUTE"
                column_name = attributes[attributes.index(attri_case) + 1]
                header = header + column_name + ","
            elif "@DATA" in line or "@data" in line:
                data = True
                header = header[:-1]
                header += '\n'
                new_content.append(header)
        else:
            new_content.append(line)
    return new_content


if __name__ == "__main__":
    # All ARFFs in directory to csv format
    files = [arff for arff in os.listdir('.') if arff.endswith(".arff")]
    # Main loop for reading and writing files
    for file in files:
        with open(file, "r") as inFile:
            content = inFile.readlines()
            name, ext = os.path.splitext(inFile.name)
            new = toCsv(content)
            with open(name + ".csv", "w") as outFile:
                outFile.writelines(new)
