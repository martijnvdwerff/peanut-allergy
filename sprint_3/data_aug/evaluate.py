"""
A module that contains training, cross-validation and
visualization functions used in the Jupyter notebook.
"""

# Information
__author__ = "Martijn van der Werff (345772) and Dennis Scheper (373689)"
__date__ = "09/05/2021"
__type__ = "Module"

# Imports
import os
import argparse
import data_aug as da
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scikitplot as skplt

from math import sqrt
from sklearn.model_selection import cross_validate, cross_val_predict, LeaveOneOut
from sklearn.metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error, r2_score

## Algorithms
from sklearn.ensemble import RandomForestRegressor
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
from sklearn.metrics.pairwise import polynomial_kernel
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor
from sklearn.linear_model import LinearRegression

# Functions
def is_unique(path):
    """
    Check whether a path is unique, otherwise make
    the filename unique by adding a number.
    This way, newly generated data can be recorded
    automatically on every run.
    """
    filename, extension = os.path.splitext(path)
    counter = 1
    while os.path.exists(path):
        path = filename + " (" + str(counter) + ")" + extension
        counter += 1
    return path
    
    
def write_csv(data, *, name='name.csv'):
    """
    Writes a new csv file containing existing and
    newly generated instances.
    """
    # Check if group column is deleted (just to make sure)
    data = data.drop('group', axis=1, errors='ignore')
    # Check if csv extension is used in name
    if not name.endswith('.csv'):
        name = name + '.csv'
    # Check whether path is unique - otherwise make it unique
    path = is_unique("/commons/Themas/Thema11/VliegBoerstra/DennisMartijn/csv_final_datasets/new_datasets/{}".format(name))
    # Write pd dataframe to csv (to commons)
    data.to_csv(path, index=False, header=True)


def train(data, model, *, scoring=[mean_absolute_error, mean_squared_error,
                                   mean_absolute_percentage_error, r2_score]):
    """
    A function that trains a model and returns the prediction and the actual class values.
    Uses a set to train the model with, and a test set to evaluate how well the model 
    does on unknown data. Uses 20% of the data as a test set. Parameters include:
        - data: dataframe
        - model: algorithm to use
        (Optional)
        - scoring: a list containing used evaluation metrics; standard used are
            - mean_absolute_error
            - mean_squared_error
            - mean_absolute_percentage_error
            - r2_score
    """
    # Split class from the dataset
    x, y = data.drop('ThresholdCumupeanutprotein', 1), data['ThresholdCumupeanutprotein']
    # Define parameters for splitting the data
    np.random.seed(0)
    indices = np.random.permutation(len(x))
    perc_split = int(len(x) * 0.2)
    # Split up in training- and testset
    x_train, y_train = x.iloc[indices[:-perc_split]], y.iloc[indices[:-perc_split]]
    x_test, y_test = x.iloc[indices[-perc_split:]], y.iloc[indices[-perc_split:]]
    # Fit the model with training data
    model.fit(x_train, y_train)
    # Make predictions based on testdata
    prediction = model.predict(x_test)
    # Construct a scoring table
    scores = [[f"{metric.__name__}", round(metric(y_train, model.predict(x_train)), ndigits=2),
               round(metric(y_test, prediction), ndigits=2)] for metric in scoring]
    # Return values with converting 'y_test' to a numpy array
    return prediction, y_test.to_numpy(), scores

    
def cross_val(data, model, *, cv=LeaveOneOut(),
              scoring=['neg_mean_absolute_error',
                       'neg_mean_squared_error',
                       'neg_root_mean_squared_error',
                       'neg_mean_absolute_percentage_error']):
    """
    Cross-validation function that uses multiple evaluation
    metrics. Training set is used to estimate how well models perform
    on unknown data with the LeaveOneOut strategy (validation); similar
    to the approach used in Weka. A test set is used in the "train()" function!
    Parameters include:
        - data: dataframe
        - model: algorithm to use
        (Optional)
        - cv: cross-validation strategy; standard is LeaveOneOut()
        - scoring: evaluation metrics used; standard used are
            - neg_mean_absolute_error
            - neg_mean_squared_error
            - neg_root_mean_squared_error
            - neg_mean_absolute_percentage_error
    """
    # Split data up
    x, y = data.drop('ThresholdCumupeanutprotein', 1), data['ThresholdCumupeanutprotein']
    # Cross-validate with resulting scoring metrics and predictions
    scores = cross_validate(model, x, y, cv=cv, scoring=scoring)
    predicted = cross_val_predict(model, x, y, cv=cv)
    #scores = [absolute(score) for score in scores]
    return scores, predicted


## Visualization functions
def visualize_dis_class(data, bins, *, type="plt"):
    """
    Visualizes the class variable based on an amount of bins/
    groups, similar to the group structure used to generate
    new data. Standard amounts vary per dataset (both types: 6,
    positives only: 5). Can visualize two different plots based 
    on the type parameter. This includes a normal histogram (standard)
    and a density plot (specified with "sns").
    """
    cls = data['ThresholdCumupeanutprotein']
    if type == "plt":
        plt.hist(cls, color = 'blue', edgecolor = 'black', bins=bins)
        # Add labels
        plt.title('Histogram of class variable, ThresholdCumupeanutprotein')
        plt.xlabel('Response threshold in pinda miligrams')
        plt.ylabel('Amount')
    elif type == "sns":
        sns.distplot(cls, hist=True, kde=True, 
                 bins=bins, color = 'darkblue', 
                 hist_kws={'edgecolor':'black'},
                 kde_kws={'linewidth': 3})
    

def visualize_training(data, model):
    """
    Visualizes the training and test set of a model by
    plotting the predicted and measured valuations against
    one another. In addition, produces a table with metric
    scores of the training and test sets. Parameters include:
        - data: dataframe
        - model: used algorithm
    """
    # Split data
    predicted, y, scores = data[0], data[1], data[2]
    fig, ax = plt.subplots()
    ax.scatter(y, predicted, edgecolors=(0, 0, 0))
    ax.plot([y.min(), y.max()], [y.min(), y.max()], 'k--', lw=4)
    ax.set_title(f'Training results gathered from the {model} function')
    ax.set_xlabel('Measured')
    ax.set_ylabel('Predicted')

    # Table
    fig, ax = plt.subplots()
    column_labels=["Metric", "Training", "Test"]
    ax.axis('tight')
    ax.axis('off')
    table = ax.table(cellText=scores, colLabels=column_labels, loc="center")
    plt.show()


def visualize_cross(data, model):
    """
    Visualizes the cross-validation of a model. Produces a
    multigraph visualizing a set of four different used metrics.
    Can be used to spot outliers easily. In addition, produces a
    table that highlights the mean score of used metrics.
    Parameters include:
        - data: dataframe
        - model: used algorithm
    """
    scores, predictions = data[0], data[1]
    fig, axs = plt.subplots(2, 2)
    fig.suptitle(f"Result score metrics of cross-validation on {model}", fontsize="x-large")
    axs[0, 0].plot(abs(scores['test_neg_mean_absolute_error']))
    axs[0, 0].set_title('Mean absolute error')
    axs[0, 1].plot(abs(scores['test_neg_root_mean_squared_error']),
                   'tab:orange')
    axs[0, 1].set_title('Root mean squared error')
    axs[1, 0].plot(abs(scores['test_neg_mean_absolute_percentage_error']),
                   'tab:green')
    axs[1, 0].set_title('Mean absolute percentage error')
    axs[1, 1].plot(predictions, 'tab:red')
    axs[1, 1].set_title('Prediction values')
    for ax in axs.flat:
        ax.set(xlabel='Instance', ylabel='Score')

    # Hide x labels and tick labels for top plots and y ticks for right plots.
    for ax in axs.flat:
        ax.label_outer()

    # Table
    fig, ax = plt.subplots()
    metrics = ['test_neg_mean_absolute_error', 'test_neg_root_mean_squared_error',
               'test_neg_mean_absolute_percentage_error']
    metric_names = ['mean_absolute_error', 'root_mean_squared_error',
                    'mean_absolute_percentage_error']
    scores = [[metric_names[i], round(np.mean(abs(scores[metric])), ndigits=2)] for i, metric in enumerate(metrics)]
    column_labels=["Metric", "Mean score"]
    ax.axis('tight')
    ax.axis('off')
    table = ax.table(cellText=scores, colLabels=column_labels, loc="center")
    plt.show()

    
def train_and_cross(data, model, *, scoring=[
                       'neg_mean_absolute_error',
                       'neg_mean_squared_error',
                       'neg_root_mean_squared_error',
                       'neg_mean_absolute_percentage_error']):
    """
    Performs a training and cross-validation process and plots the
    scores per instance on a given metric. Allows for comparing the
    training and cross-validation on overfitting. Plots per given metric.
    Parameters include:
        - data: dataframe
        - model: used algorithm
        (Optional)
        - Scoring: metrics to measure performance; standard metrics used are
            - neg_mean_absolute_error
            - neg_mean_squared_error
            - neg_root_mean_squared_error
            - neg_mean_absolute_percentage_error
    """
    x, y = data.drop('ThresholdCumupeanutprotein', 1), data['ThresholdCumupeanutprotein']
    # Plot for each scoring metric
    for i in range(len(scoring)):
        skplt.estimators.plot_learning_curve(
        model, x, y, cv=LeaveOneOut(), shuffle=False, scoring=scoring[i],
        n_jobs=-1, figsize=(6, 4), title_fontsize="large",
        text_fontsize="large", title=f"{model} Learning Curve based on {scoring[i]}")

def main():
    """
    Main function. Only for testing purposes!
    """
    # NOTE: This is all test data, look for the Jupyter notebook
    #       for an in-depth explanation and many visualizations!
    
    # Declare amount of instances for data augmentation
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-i', "--instances", required=True, type=int,
                        help="Amount of instances", metavar='')
    args = parser.parse_args()
    # Load in existing data; replace '?' (former NaNs) to zero
    both_types = pd.read_csv("/commons/Themas/Thema11/VliegBoerstra/DennisMartijn/csv_final_datasets/both_types.csv") \
        .replace("?", 0).apply(pd.to_numeric, errors='ignore')
    pos_only = pd.read_csv("/commons/Thema11/VliegBoerstra/DennisMartijn/csv_final_datasets/positives_only.csv") \
        .replace("?", 0).apply(pd.to_numeric, errors='ignore')
    # Generate new instances
    new_pos = da.generate_instances(pos_only, index=5, instances=args.instances)
    new_both = da.generate_instances(both_types, index=11, instances=args.instances)
    # Record newly generated instances by writing to a CSV
    write_csv(new_both, name=f'both_{args.instances}')
    write_csv(new_pos, name=f'pos_{args.instances}')
    # Test on multiple algorithms at once, with the same settings as used in Weka
    models_both = [RandomForestRegressor(), SVR(kernel='poly', epsilon=0.001), GPR(kernel=DotProduct() + WhiteKernel())]
    models_pos = [SVR(kernel='poly', epsilon=0.001), GPR(kernel=DotProduct() + WhiteKernel()), LinearRegression()]
    # Training and cross-validation
    train_both = [train(new_both, model=model) for model in models_both]
    train_pos = [train(new_pos, model=model) for model in models_pos]
    cross_both = [cross_val(new_both, model=model) for model in models_both]
    cross_pos = [cross_val(new_pos, model=model) for model in models_pos]
    # Basic visualization (training only) on one model for each dataset
    visualize_training(train_both[0])
    visualize_training(train_pos[0])

if __name__ == "__main__":
    main()
