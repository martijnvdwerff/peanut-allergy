"""
This module contains some data augmentation functions
that generate new instances based on existing ones.

This is the updated version! We now have two wrappers:
    - The "old" method: based on standard deviation per group; and
    - The "new" method: based on a covariance matrix per group.
"""

# Information
__author__ = "Martijn van der Werff (345772) and Dennis Scheper (373689)"
__date__ = "07/05/2021"
__type__ = "Module"

# Imports
import pandas as pd
import numpy as np
import random
import warnings

# Import to ignore some warnings that do not matter for the performance
from pandas.core.common import SettingWithCopyWarning

# Functions
def wrapper_std(data, index, func, instances, groups):
    """
    The 'old' wrapper, which is still the standard option to go with,
    but does not account for correlation between variables/attributes
    present in the datasets. Therefore, a new wrapper function has been
    established to account for this issue. Results from this version
    of the wrapper will be compared to the new function to prove which one
    is 'better'. Parameters include:
        - data: dataset that will be used to generate new instances
        - index: distinction/split point for categorical and numeric attributes
        - func: distribution function to be used for numeric attributes
        - instances: the amount of instances to be generated
        - groups: the amount of group needed to be defined (on the class var)
    """
    # Distinction between categorial and numeric
    groups = shape_groups(data, groups=groups)
    # Add group column (last column) to the categorical subset
    categorical_set = pd.concat([groups.iloc[:, : index], groups['group']], axis=1)
    # Define a numeric group
    numeric_set = groups.iloc[:, index:]
    # Calculate probability
    perc_unique = [[(group[col].unique(), group[col].value_counts()/len(group)) for col in group if col != 'group']
                   for name, group in categorical_set.groupby(by='group')]
    # Calculate sd and mu
    mu = [[np.mean(group[col]) for col in group if col != 'group'] for name, group in numeric_set.groupby(by='group')]
    sd = [[np.std(group[col]) for col in group if col != 'group'] for name, group in numeric_set.groupby(by='group')]
    # Drop group from a new dataframe
    new_data = pd.DataFrame(columns=groups.columns).drop('group', 1)
    for i in range(instances):
        # Pick a random group each time
        n = random.randint(0, len(mu) - 1)
        # Add to a growing dataframe
        new_cat = [random.choices(counts, weights=percentage) for counts, percentage in perc_unique[n]]
        new_cat = [item for sub_list in new_cat for item in sub_list]    
        new_num = [round(func(mu[n][a], sd[n][a]), ndigits=6).clip(min=0) for a in range(len(mu[n]))]
        # Add a growing to dataframe
        new_data.loc[len(new_data)] = new_cat + new_num
    return new_data


def wrapper_correlation_r(data, index, func, instances, groups):
    """
    The 'new' wrapper, which is callable through the wrapper parameter
    in the 'generate_instance' function. Calculates and establishes a
    covariance matrix per defined group.
    Parameters include:
        - data: dataset that will be used to generate new instances
        - index: distinction/split point for categorical and numeric attributes
        - func: distribution function to be used for numeric attributes
        - instances: the amount of instances to be generated
        - groups: the amount of group needed to be defined (on the class var)
    """
    groups = shape_groups(data, groups=groups)
    # Calculate sd and mu
    mu = [[np.mean(group[col]) for col in group if col != 'group'] for name, group in groups.groupby(by='group')]
    # Define a covariance matrix -- per group or per group and per instance or per instance
    r = [np.cov(group.drop('group', 1), rowvar=False) for name, group in groups.groupby(by='group')]
    new_data = pd.DataFrame(columns=groups.columns).drop('group', 1)
    for i in range(instances):
        n = random.randint(0, len(mu) - 1)
        # Use the multivariate function to generate a new instance
        new_instance = np.random.multivariate_normal(mu[n], r[n], size=1).clip(min=0)
        new_instance = [item for sub_list in new_instance for item in sub_list]
        # Round to whole numbers (categorical) - different due to different implementation; functionally the same
        new_instance[:index] = [round(item) for item in new_instance[:index]]
        # Add a growing to dataframe
        new_data.loc[len(new_data)] = new_instance
    return new_data

        
def shape_groups(data, groups):
    """
    Determine groups on the class variable. Negatives will be one,
    big group, whereas positive provocations are subdued in an n
    amount of groups (standard is 5).
    """
    # Determine the amount of unique valuations in the class
    distinct_type = data['ThresholdCumupeanutprotein'].unique()
    if len(distinct_type) == 1:
        # Negatives
        data['group'] = '9.6'
    else:
        # Positives
        data['group'] = pd.qcut(data['ThresholdCumupeanutprotein'], q=groups)
    return data
    

def generate_instances(data, index, *, func=random.gauss, instances=100, groups=5, wrapper=wrapper_std):
    """
    A function that generates new instances by directing and calling upon other functions.
    Main focus is on treating dataframes
    Parameters:
        - data: a dataframe
        - index: distinction between categorical and numeric data
        (Optional)
        - func: function that is used for generating new, numeric columns
        - instances: the amount of new instances
        - groups: amount of groups for splitting the class in
        - wrapper: which wrapper to use (each uses a different rule set); standard is 'wrapper_std'
    """
    warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
    if index == 11:
        # Distribute instances over two function calls
        instances = round(instances/2)
        # Split the data over positive and negative provocations
        mask = data['ThresholdCumupeanutprotein'] == 9.615805
        data_neg = wrapper(data[mask], index=index, func=func, instances=instances, groups=groups)
        data_pos = wrapper(data[~mask], index=index, func=func, instances=instances, groups=groups)
        new_instances = pd.concat([data_neg, data_pos], axis=0)
    else:
        new_instances = wrapper(data, index=index, func=func, instances=instances, groups=groups)
    data = pd.concat([data, new_instances], axis=0)
    # Return extended dataframe without the group column (if exists)
    return data.drop('group', 1, errors='ignore')


if __name__ == "__main__":
    # Test data on both types; evaluation in other script
    both_types = pd.read_csv("/commons/Thema11/VliegBoerstra/DennisMartijn/csv_final_datasets/both_types.csv")\
        .replace("?", 0).apply(pd.to_numeric, errors='ignore')
    # Run with standard settings and ten instances
    new_dataset = generate_instances(both_types, index=11, instances=10)
    print("=== Existing ({} instances) and newly ({} instances) generated instances ===".format(
        len(both_types), len(new_dataset)-len(both_types)))
    print(new_dataset)
